'use strict';

var _ = require('lodash');
var jsonfile = require('jsonfile');
var path = require('path');
var FILE = path.resolve('db', 'products.json');
var bodyParser = require('body-parser');

// Get list of products
exports.get = function(req, res) {
  jsonfile.readFile(FILE, function (err, obj) {
    var array = _.reduce(_.map(obj.categories), function (result, arr) {
      return result.concat(arr);
    }, []);

    var result = {};
    if(req.query.type){
      result = _.find(array, function (item) {
        return item.type == req.query.type;
      });
    }else{
      result = {result : array};
    }


    if (!result) {
      res.status(404).send('Error 404');
    } else {
      res.status(200).type('json').json(result);
    }
  });
};

exports.getProduct = function(req, res){
  jsonfile.readFile(FILE, function (err, obj) {

    var array = _.reduce(_.map(obj.categories), function (result, arr) {
      return result.concat(arr);
    }, []);

    var result = {};
    if(req.query.type){
      result = _.find(array, function (item) {
        return item.type == req.query.type ;
      });

      result =  _.find(result.products,function(value){
        return value.id === req.params.id;
      });
    }else{
      result = {result : array};
    }

    if (!result) {
      res.status(404).send('Error 404');
    } else {
      res.status(200).type('json').json(result);
    }
  });
}

exports.put = function (req, res, next) {
  jsonfile.readFile(FILE, function (err, obj) {

    /*var array = _.reduce(_.map(obj.categories), function (result, arr) {
      return result.concat(arr);
    }, []);*/

    var result = null;
    if(req.query.type){
      _.forEach(obj.categories, function (item) {

        if(item.type == req.query.type){
          _.forEach(item.products,function(value){
            if(value.id == req.params.id){
              value.mark = req.body.mark;
              value.model = req.body.model;
              value.price = req.body.price.toString(),
              value.InitDate = req.body.InitDate;
              result = value;
            }
          })
        }
      });

    }

    if(!result){
      result = {};
      res.status(404).send('Error 404').json(result);
    }
    else{

      setTimeout(function () {
        jsonfile.writeFile(FILE, {categories: obj.categories}, function (err) {
         //res.status(500).send('Error 500');
         });
        res.status(200).type('json').json(result);
         }, 2000);//force delay of 2 seconds.

    }

  })
  ;
}
