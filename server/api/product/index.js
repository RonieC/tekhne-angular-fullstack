'use strict';

var express = require('express');
var controller = require('./product.controller');

var router = express.Router();

router.get('/', controller.get);
router.get('/:id',controller.getProduct);
router.put('/:id', controller.put);


module.exports = router;
