'use strict';

var _ = require('lodash');
var jsonfile = require('jsonfile');
var path = require('path');
var FILE = path.resolve('db', 'users.json');
var bodyParser = require('body-parser');

// Get list of users
exports.post = function(req, res) {

  jsonfile.readFile(FILE, function (err, obj) {
    var array = _.reduce(_.map(obj.users), function (result, arr) {
      return result.concat(arr);
    }, []);

    var result = _.find(array, function (item) {
      return item.name == req.query.username && item.password == req.query.password ;
    });
    if (!result) {
      res.status(404).send('Error 404');
    } else {
      res.status(200).type('json').json(result);

//            res.type('.html');              // => 'text/html'
//            res.type('html');               // => 'text/html'
//            res.type('json');               // => 'application/json'
//            res.type('application/json');   // => 'application/json'
//            res.type('png');                // => image/png:
    }
  });
};
