'use strict';

angular.module('proyectoFullstackApp', [
  'angular-momentjs',
  'cgBusy',
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngLodash',
  'angularLocalStorage',
  'ngCookies',
  'ui.router',
  'ui.bootstrap'
  ,  'ngStorage'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  });
