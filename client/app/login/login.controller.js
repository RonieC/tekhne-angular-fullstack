'use strict';

angular.module('proyectoFullstackApp')
  .controller('LoginCtrl', function ($state, $scope, $http, User) {
    $scope.message = 'Hello';
    $scope.user = {
      name: '',
      password: ''
  };



    $scope.submit = function () {
      $scope.promise = User.post($scope.user);
      $scope.promise.then(function(response){
        $state.go('category');
      });
    };
  });
