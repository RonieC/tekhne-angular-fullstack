'use strict';

angular.module('proyectoFullstackApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('product-delete', {
        url: '/product-delete',
        templateUrl: 'app/category/product-delete/product-delete.html',
        controller: 'ProductDeleteCtrl'
      });
  });