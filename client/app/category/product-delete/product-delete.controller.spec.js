'use strict';

describe('Controller: ProductDeleteCtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var ProductDeleteCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProductDeleteCtrl = $controller('ProductDeleteCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
