'use strict';

angular.module('proyectoFullstackApp')
  .controller('ProductDeleteCtrl', function ($scope,$uibModalInstance, product) {
    var vm = this;
    vm.selected = product;

    vm.ok = function () {
      $uibModalInstance.close(vm.selected);
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
