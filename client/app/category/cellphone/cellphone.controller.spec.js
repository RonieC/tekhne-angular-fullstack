'use strict';

describe('Controller: CellphoneCtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var CellphoneCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CellphoneCtrl = $controller('CellphoneCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
