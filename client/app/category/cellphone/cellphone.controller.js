'use strict';

angular.module('proyectoFullstackApp')
  .controller('CellphoneCtrl', function ($scope,Product, User,lodash) {
    var  vm = this;
    vm.deleteProduct = deleteProduct;
    vm.editProduct= editProduct;
    vm.permissions = User.getPermissions();
    vm.products = null;
    activate();

    function activate() {
      vm.promise =  Product.get('cellphones');
      vm.promise.then(function (response) {
        vm.products = response.products;
         $scope.$parent.vm.products = vm.products;
      });
    }

    $scope.$on('category.cellphones', function () {
      activate();
    });

    function deleteProduct(productId){
      $scope.$parent.vm.deleteProduct(getProduct(productId));
    }

    function editProduct(productId){
      $scope.$parent.vm.editProduct(getProduct(productId),'cellphones');
    }

    function getProduct(productId){
      var pro = lodash.clone(lodash.find(vm.products,function(value){
        return value.id === productId;
      }),true);
      pro.fecha = new Date(pro.InitDate);
      return pro;
    }

  });
