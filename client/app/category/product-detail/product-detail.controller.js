'use strict';

angular.module('proyectoFullstackApp')
  .controller('ProductDetailCtrl', function ($scope, $uibModalInstance, product) {
    var vm = this;
    vm.selected = product;

   /* vm.selected = {
      item: vm.items[0]
    };*/

    vm.ok = function () {
      $uibModalInstance.close(vm.selected);
    };

    vm.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
