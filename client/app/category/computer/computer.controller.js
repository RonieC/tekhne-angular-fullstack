'use strict';

angular.module('proyectoFullstackApp')
  .controller('ComputerCtrl', function ($scope, Product, User,lodash) {
    var vm = this;
    vm.deleteProduct = deleteProduct;
    vm.editProduct= editProduct;
    vm.permissions = User.getPermissions();
    vm.products =  $scope.$parent.vm.products;


    function activate() {
      vm.promise = Product.get('computers');
      vm.promise.then(function (response) {
        vm.products = response.products;
         $scope.$parent.vm.products = vm.products;
      });
    }

    $scope.$on('computers', function () {
      activate();
    });

    function deleteProduct(productId){
      $scope.$parent.vm.deleteProduct(getProduct(productId));
    }

    function editProduct(productId){
      $scope.$parent.vm.editProduct(getProduct(productId),'computers');
    }

    function getProduct(productId){
     var pro = lodash.clone(lodash.find(vm.products,function(value){
        return value.id === productId;
      }),true);
      pro.fecha = new Date(pro.InitDate);
      return pro;
    }
  });
