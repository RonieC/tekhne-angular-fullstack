'use strict';

describe('Controller: ComputerCtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var ComputerCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ComputerCtrl = $controller('ComputerCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
