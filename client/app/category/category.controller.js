'use strict';

angular.module('proyectoFullstackApp')
  .controller('CategoryCtrl', function ($scope,Product,$uibModal,$log,lodash,User) {
    var vm = this;
    vm.categories = null;
    vm.CATEGORY = {
      COMPUTER: {
        key: 'computers'
      },
      CELLPHONE: {
        key: 'cellphones'
      }
    };
    vm.deleteProduct = deleteProduct;
    vm.editProduct = editProduct;
    vm.products = null;
    vm.permissions = User.getPermissions();
    vm.setTab = setTab;
    vm.tab = {
      computers: {
        active: true
      },
      cellphones: {
        active: false
      }
    };

    vm.currentCategory = vm.CATEGORY.COMPUTER.key;


    activate();

    function activate(){

      loadComputers();
    }

    function setTab(tab){
      if (vm.CATEGORY.COMPUTER.key === tab.key) {
        vm.tab.computers.active = true;
        vm.tab.cellphones.active = false;
        vm.currentCategory = vm.CATEGORY.COMPUTER.key;
        loadComputers();
      }
      else {
        vm.currentCategory = vm.CATEGORY.CELLPHONE.key;
        vm.tab.computers.active = false;
        vm.tab.cellphones.active = true;
        loadCellPhones();
      }

     // $scope.$broadcast(tab.key);
    }

    function loadComputers(){
      vm.promise = Product.get('computers');
      vm.promise.then(function (response) {
        vm.products = response.products;
      });
    }

    function loadCellPhones(){
      vm.promise =  Product.get('cellphones');
      vm.promise.then(function (response) {
        vm.products = response.products;

      });
    }

    function editProduct (productId){
      var product = getProduct(productId)
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/category/product-detail/product-detail.html',
        controller: 'ProductDetailCtrl as proDetail',
        size: 'lg',
        resolve: {
          product: function () {
            product.price = parseInt(product.price);
            return product;
          }
        }
      });

      modalInstance.result.then(function (product) {
       vm.promise = Product.saveChanges(vm.currentCategory,product.id, product);
        vm.promise.then(function (response) {
         lodash.forEach(vm.products,function(value){
           if(value.id === response.id){
             value.mark = response.mark;
             value.model =response.model;
             value.price =parseInt(response.price);
             value.InitDate =response.InitDate;
             value.fecha = new Date(response.InitDate);


 }
         })
        });
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    }


    function deleteProduct(productId){
      var product = getProduct(productId)
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/category/product-delete/product-delete.html',
        controller: 'ProductDeleteCtrl as proDelete',
        size: 'lg',
        resolve: {
          product: function () {
            return product;
          }
        }
      });

      modalInstance.result.then(function (product) {
        console.log(product,vm.products);
        lodash.remove(vm.products,function(value){
          return product.id === value.id;
        });
      }, function () {
        $log.info('Modal Delete dismissed at: ' + new Date());
      });
    }

    function getProduct(productId){
      var pro = lodash.clone(lodash.find(vm.products,function(value){
        return value.id === productId;
      }),true);
      pro.fecha = new Date(pro.InitDate);
      return pro;
    }
  });
