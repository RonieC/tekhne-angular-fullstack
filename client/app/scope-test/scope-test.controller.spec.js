'use strict';

describe('Controller: ScopeTestCtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var ScopeTestCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ScopeTestCtrl = $controller('ScopeTestCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
