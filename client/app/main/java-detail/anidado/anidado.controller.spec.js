'use strict';

describe('Controller: AnidadoCtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var AnidadoCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AnidadoCtrl = $controller('AnidadoCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
