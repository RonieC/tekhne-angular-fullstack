'use strict';

angular.module('proyectoFullstackApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main.angular', {
        url: 'angular',
        templateUrl: 'app/main/angular/angular.html',
        controller: 'AngularCtrl as angular'
      });
  });