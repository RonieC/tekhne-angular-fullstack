'use strict';

angular.module('proyectoFullstackApp')
  .service('User', function ($q, $resource) {
    // AngularJS will instantiate a singleton by calling "new" on this function

    var getPermissions,
      permissions,
      post,
      resource,
      setPermissions,
      getUsername,
      setUser,
      user={};

    resource = $resource('api/users/login', {username:'@username',password:'@password'},
      {
        login: {method: 'POST'}
      });
   // "permissions": ["edit", "delete"]
    permissions = null;

    post = function(credentials) {
      var userService = $q.defer();
      resource.login(
        {
          username: credentials.name,
          password: credentials.password
        }).$promise.then(function (response){
          userService.resolve(response);
          setPermissions(response);
          setUser(response);

        },function(error){
          userService.reject(error);
        });

      return userService.promise;
    }

    setPermissions = function (user){

      permissions={};
      angular.forEach(user.permissions,function(value, index){
        if(value === 'edit' ){
          permissions.hasEdit = true;
        }
        else if(value === 'delete' ){
          permissions.hasDelete = true;
        }
      })
    }

    setUser = function(userDatas){
      user.name = userDatas.name;
    }

    getPermissions = function(){
      return permissions;
    }

    getUsername= function(){
      return user.name;
    }


    return {
      post: post,
      getPermissions: getPermissions,
      getUsername: getUsername
    };
  });
