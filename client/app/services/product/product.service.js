'use strict';

angular.module('proyectoFullstackApp')
  .service('Product', function ($q, $resource) {

    var resource = $resource('/api/products/:id', {type: '@type',id:'@id'},{
      saveChanges: {
        method: 'PUT'
      }
    });

    var get = function (type,id) {
      var product = $q.defer();

      resource.get({type: type, id:id}).$promise.then(function (response) {
        product.resolve(response);
      }, function (error) {
        product.reject(error);
      });

      return product.promise;
    };

    var put = function(type, id, payload) {
      var product = $q.defer();

      resource.saveChanges({type: type,id: id}, payload).$promise.then(function (response) {
        product.resolve(response);
      }, function (error) {
        product.reject(error);
      });

      return product.promise;
    }

    return {
      get: get,
      saveChanges: put
    };
  });
